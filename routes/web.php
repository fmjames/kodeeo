<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Usercontroller@index');
Route::get('/home','sidecontroller@master')->name('master');
Route::get('/orders','sidecontroller@orders')->name('orders');

Route::get('/products','productscontroller@products')->name('products');
Route::post('/products-create','productscontroller@create')->name('products-create');
Route::get('/products-list','productscontroller@products_list')->name('products-list');
Route::get('/products/{productId}/edit','productscontroller@edit')->name('product-edit');
Route::post('/products/{productId}/update','productscontroller@update')->name('product-update');
Route::get('/products/{productId}/delete','productscontroller@delete')->name('product-delete');

Route::get('/category','categorycontroller@category_view')->name('category');
Route::post('/category/create','categorycontroller@category_create')->name('create-category');
Route::get('/category/{categoryId}/edit','categorycontroller@edit')->name('category-edit');
Route::post('/category/{categoryId}/update','categorycontroller@update')->name('category-update');
Route::get('/category/{categoryId}/delete','categorycontroller@delete')->name('category-delete');


// Route::get('/me', function () {
//     return view('welcomeme');
// });
