@extends('master')
@section('content')


<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card card-default">

				<div class="card-header">
					<h3 align="center">Products List</h3>
				</div>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Category-ID</th>
					<th>Price</th>
					<th>Action</th>
				</tr>

				<div class="card-body">
					@foreach($all_data as $key=>$single_data)
					<tr>
						<th>{{$single_data->id}}</th>
						<th>{{$single_data->name}}</th>

                            <th> {{$single_data->category_id}}</th>

						<th>{{$single_data->price}}</th>
                        <th>
                            <a href="/products/{{$single_data->id}}/edit" class="btn btn-info">Update</a>
                            <a href="/products/{{$single_data->id}}/delete" class="btn btn-danger">Delete</a>
                        </th>

					</tr>
					@endforeach
				</div>


			</div>
		</div>
	</div>

</div>

@endsection
