@extends('master')
@section('content')

    <div class="container">
        <h3>Update Product</h3>

        <form action="/products/{{$products->id}}/update" method="post" role="form" id="product_form">
            @csrf
            <div class="form-group">
                <label for="p_name">Update Product Name</label>
                <input type="text" name="product_name" id="p_name" placeholder="Update Product Name" class="form-control" value="{{$products->name}}">
            </div>
            <div class="form-group">
                <label for="p_price">Update Product Price</label>
                <input type="number" name="product_price" id="p_price" placeholder="Update Product Price" class="form-control" value="{{$products->price}}">
            </div>
            <div class="form-group">
                <label for="c_list">Update Category</label>
                <select name="product_category" class="form-control" id="c_list">
                    @foreach($all_data as $single_data)
                        <option value="{{$single_data->id}}">{{$single_data->name}} </option>
                    @endforeach
                </select>
            </div>

        </form>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" form="product_form">Submit</button>
        </div>

    </div>

@endsection
