@extends('master')
@section('content')


<a type="button" class="btn btn-success float-right" href="{{route('products-list')}}">Products List</a>

<div class="container">
  <h3>Create Product</h3>
  
  <form action="{{route('products-create')}}" method="post" role="form" id="product_form">
    @csrf
    <div class="form-group">
    <label for="p_name">Enter Product Name</label>
    <input type="text" name="product_name" id="p_name" placeholder="Enter Product Name" class="form-control">
    </div>
    <div class="form-group">
    <label for="p_price">Enter Product Price</label>
    <input type="number" name="product_price" id="p_price" placeholder="Enter Product Price" class="form-control">
    </div>
    <div class="form-group">
    <label for="">Select Category</label>
    <select name="product_category" class="form-control">
      @foreach($all_data as $single_data)
      <option value="{{$single_data->id}}">{{$single_data->name}}</option>
      @endforeach
    </select>
    </div>

  </form>
  <div class="form-group">
    <button type="submit" class="btn btn-primary" form="product_form">Submit</button>
  </div>

</div>


@endsection