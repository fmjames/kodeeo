@extends('master')
@section('content')

<h3>Category List</h3>

<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createcategory">Create new Category</button>

<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Category Name</th>
      <th scope="col">Status</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach($all_data as $key=>$single_data)
    <tr>
      <th>{{$key+1}}</th>
      <th>{{$single_data->name}}</th>
      <th>{{$single_data->status}}</th>
        <th>
            <a href="/category/{{$single_data->id}}/edit" class="btn btn-info">Update</a>
            <a href="/category/{{$single_data->id}}/delete" class="btn btn-danger">Delete</a>
        </th>

    </tr>
    @endforeach



  </tbody>
</table>




<div class="modal" tabindex="-1" role="dialog" id="createcategory">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Category details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('create-category')}}" method="post" role="form" id="createform">
        	@csrf
        	<div>
        		<label for="c_name">Enter Category Name:</label>
        	<input type="text" name="category_name" id="c_name" placeholder="Category Name" class="form-control" required=""></input>
        	</div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" form="createform">Save</button>
        <!-- <input type="submit" name="save" > -->
      </div>
    </div>
  </div>
</div>

@endsection
