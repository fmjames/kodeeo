@extends('master')
@section('content')
<h3>Update Category</h3>
    <form action="/category/{{$category->id}}/update" method="post" role="form" id="createform">
        @csrf
        <div class="form-group">
            <label for="c_name">Update Category Name:</label>
            <input type="text" name="category_name" value="{{$category->name}}" id="c_name" placeholder="Category Name" class="form-control" required></input>
        </div>
    </form>

    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary" form="createform">Update</button>
    </div>

@endsection
