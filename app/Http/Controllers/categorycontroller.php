<?php

namespace App\Http\Controllers;
use App\Models\Category;
use App\Models\Products;
use Illuminate\Http\Request;

class categorycontroller extends Controller
{
    //
    public function category_view(){
    	$all_data = Category::all();

    	return view('elements.category', compact('all_data'));
    }

    public function category_create(Request $request){

    	Category::create([
    		'name'=>$request->category_name
    	]);
    	return redirect()->back();

    }

    public function edit($categoryId){

        return view('/elements/category-update')->with('category',Category::find($categoryId));

    }

    public function update($categoryId){
        $data=request()->all();
        $category=Category::find($categoryId);
        $category->name=$data['category_name'];
        $category->save();
        return redirect('/category');
    }

    public function delete($categoryId){
        $category=Category::find($categoryId);
        $category->delete();
        return redirect('/category');
    }
}
