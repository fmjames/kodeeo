<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Products;
use App\Models\Category;

class productscontroller extends Controller
{
    //
    public function products_list(){
      $all_data=Products::all();
//      dd($all_data);
      return view('elements.products-list',compact('all_data'));
    }


     public function products()
   {
   	$all_data=Category::all();
   	return view('elements.products',compact('all_data'));
   }


   public function create(Request $request){

//dd($request->all());
      Products::create([
        'name'=>$request->product_name,
        'price'=>$request->product_price,
        'category_id'=>$request->product_category
      ]);
      return redirect()->route('products-list');

   }

   public function edit($productId){

        $all_data = Category::with('Products')->get();
        $products= Products::find($productId);
//        dd($all_data);

        return view('elements.productupdate',compact('all_data', 'products'));
   }

   public function update($productId){
//        dd($productId);

        $data=request()->all();
        $product=Products::find($productId);
        $product->name=$data['product_name'];
        $product->price=$data['product_price'];
        $product->category_id=$data['product_category'];
        $product->save();
        return redirect('/products-list');

   }

   public function delete($productId){

        $product=Products::find($productId);
        $product->delete();
        return redirect('/products-list');

   }


}
